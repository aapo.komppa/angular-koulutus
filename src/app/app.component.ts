import { Component, OnInit } from '@angular/core';
import { timer } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  piilotettu = false;

  list: { title: string, body?: string }[] = [
    { title: 'olen otsikko', body: 'olen sisältö' },
    { title: 'olen otsikko2', body: 'olen sisältö2' },
    { title: 'olen otsikko3' },
    { title: 'olen otsikko4' },
  ];

  constructor(){}

  ngOnInit(): void {
    const abc = timer(2000).subscribe(val => {
      console.log('value of subscribe: ', val);
      this.list.push({ title: 'olen otsikko5', body: 'olen sisältö3' });
    });
  }
}
