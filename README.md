# Ajetut komennot so far:

### ng new angular-koulutus
luo ja initialisoi ng projektin, sisältää .gitignoret ja tarvittavat npm paketit

### npm install @angular/flex-layout --save
lisää flexbox tuen

### ng generate component header
### ng generate component list
lisäävät komponentit

# tehtävä:


## tee oma angular projekti jossa:

### on ainakin nämä:
- *ngIf
- *ngFor
- (click)
- fxFlex
- @Input

### käytetään ainakin 1 seuraavista asioista:
- ng-container
- ng-template
- ng-content

### kutsutaan jotakin vähintään yhdessä näistä lifecycle metodeista:
- ngOnInit
- ngOnChanges
- ngOnDestroy

### jos haluaa hifistellä
- eventEmitter